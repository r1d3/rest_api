# Description

This project is the web API of the Agayon project. See https://www.agayon.be

It can be used to:

 * Control the robot
    + forward, backward, turn
 * Start and stop the streaming
 * In the future: get information about the surroundings (obstacles, sensors)

# Install

This is a standard Flask projet, it can be deployed with a virtualenv.
R1D3 serve it with [NGINX](https://www.nginx.com) web server and [UWSGI](https://github.com/unbit/uwsgi).

The following command should be enough to install all dependecies.


```
pip install -r requirements.txt
```


