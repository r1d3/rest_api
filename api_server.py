# -*- coding: utf-8 -*-

from api_agayon import agayon_app

if __name__ == '__main__':
    agayon_app.app.run(debug=True, port=3031)
