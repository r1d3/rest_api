# -*- coding: utf-8 -*-

import socket
import os
import logging

logger = logging.getLogger('websocket')

SOCKET = "/tmp/agayon_socket"


class AgayonSocket:
    def __init__(self):
        self.client = None

    def connect(self):
        if os.path.exists(SOCKET):
            self.client = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
            try:
                self.client.connect(SOCKET)
                return True
            except ConnectionRefusedError:
                logger.error("Connection Refused")
                return False

    def send_data(self, data):
        try:
            self.client.send(data.encode('utf-8'))
            return True
        except (OSError, AttributeError) as e:
            logger.error(f"Could not send data to R1D3 UNIX socket: {str(e)}")
            return False
