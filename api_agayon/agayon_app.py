# -*- coding: utf-8 -*-

from flask import Flask, request, jsonify, render_template, redirect
from flask_restful import reqparse, abort, Api, Resource
import logging

from .socket_client import AgayonSocket

logger = logging.getLogger('Flask_api')

app = Flask(__name__)
api = Api(app)

ACTIONS = {
    'up': {'direction': 'forward', 'status': True, 'type': 'direction'},
    'down': {'direction': 'backward', 'status': True, 'type': 'direction'},
    'left': {'direction': 'left', 'status': True, 'type': 'direction'},
    'right': {'direction': 'right', 'status': True, 'type': 'direction'},
    'smallRight': {'direction': 'smallRight', 'status': True, 'type': 'direction'},
    'smallLeft': {'direction': 'smallLeft', 'status': True, 'type': 'direction'},
    'stopRobot': {'direction': 'stopRobot', 'status': True, 'type': 'direction'},
    'stopStream': {'action': 'stopStream', 'status': True, 'type': 'action_btn', 'desc': 'Stream stopped'},
    'startStream': {'action': 'startStream', 'status': True, 'type': 'action_btn',
                     'desc': 'Stream started... Please wait a few seconds'},
    'RPIrecord': {'action': 'RPIrecord', 'status': True, 'type': 'action_btn', 'desc': 'Movie is recorded from RPI. No capture available'},
    'lidarMapping': {'action': 'lidarMapping', 'status': True, 'type': 'silent_action', 'desc': 'Lidar Mapping'},
}


def abort_if_action_doesnt_exist(action_id):
    if action_id not in ACTIONS:
        abort(404, message="Action {} doesn't exist".format(action_id))


parser = reqparse.RequestParser()
parser.add_argument('actions')


# Action
# shows a single action item
class Action(Resource):

    def __init__(self):
        self.socket = AgayonSocket()
        self.is_connected = self.socket.connect()

    def get(self, action_id):
        abort_if_action_doesnt_exist(action_id)
        return ACTIONS[action_id]

    def post(self, action_id):
        if not self.is_connected:
            # try to connect everytime something is posted
            self.is_connected = self.socket.connect()
        ret = self.socket.send_data(action_id)
        if ret:
            action_type = ACTIONS[action_id].get('type')
            if action_type == 'action_btn':
                # Button that redirect to another page
                desc = ACTIONS[action_id].get('desc')
                url = "/api/view/{}".format(desc)
                logger.info(f"Action: {url}")
                return redirect(url)
            else:
                logger.info(action_id)
                return ACTIONS[action_id]
        else:
            # return {'status': False, 'error_message': 'Connection Error, Transport endpoint is not connected'}
            desc = "Main Agayon R1D3 program is not launched"
            url = "/api/view/{}".format(desc)
            return redirect(url)


# Directions
# shows a list of all directions
class Directions(Resource):

    def get(self):
        return ACTIONS


api.add_resource(Directions, '/api/actions')
api.add_resource(Action, '/api/actions/<action_id>')


@app.route('/api')
def index():
    return render_template('api.html')


@app.route('/api/view/<parameters>')
def action_render(parameters):
    return render_template('action.html', parameters=parameters)


# @app.route('/api/restart_bot')
# def restart_bot():
#     # restart_bot()
#     return render_template('restart_bot.html')

if __name__ == '__main__':
    app.run(debug=True, port=8000)
